class Model {
    constructor(name, surname, email, password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }
}


class View {
    constructor(model, controller) {
        const self = this;
        const txtPassword1 = document.getElementById('password1');
        const txtPassword2 = document.getElementById('password2');
        const form = document.getElementsByTagName('form')[0];

        self.controller = controller;

        form.onsubmit = function () {
            if ( txtPassword1.value != txtPassword2.value ) {
                return false
            }

            self.save()
        }
    }

    save() {
        event.preventDefault();
        const txtName = document.getElementById('name');
        const txtSurname = document.getElementById('surname');
        const txtEmail = document.getElementById('email');
        const txtPassword = document.getElementById('password1');

        const data = {
            first_name: txtName.value,
            last_name: txtSurname.value,
            email: txtEmail.value,
            password1: txtPassword.value,
            password2: txtPassword.value
        }

        this.controller.save(data)
    }
}


class Controller {
    initialize(model, view) {
        this.model = model;
        this.view = view;
    }

    handleErrors(response) {
        if ( !response.ok ) {
            throw Error(response.statusText);
        }

        return response
    }

    save(data) {
        fetch('https://tllega.jeremiasenriquez.com/api/auth/registration/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
              }
        })
        .then(this.handleErrors)
        .then(res => res.json())
        .catch(err => alert(err))
        .then(res => {
            localStorage.setItem('token', res['key']);
            location.href = '/profile.html'
        })
    }
}

window.onload = function () {
    const model = new Model();
    const controller = new Controller();
    const view = new View(model, controller);

    controller.initialize(model, view);
}

