class Model{
    constructor() {
        this.categories = []
        this.actions = []
    }
}


class View{
    constructor(model, controller) {
        const self = this;
        this.app = this.getElement('.box_categories');
        

        self.controller = controller;
        self.model = model;

        this.displayElements()
        self.controller.retrieve();
    }

    displayElements() {
        this.model.categories.forEach(category => {
            let column = this.createElement('div', ['col-md-6', 'col-xl-3', 'mb-4', 'item-category'])
            let border = this.createElement('div', ['card', 'shadow', 'border-left-primary', 'py-2'])
            let card = this.createElement('div', ['card-body'])
            let innerColumns = this.createElement('div', ['row', 'align-items-center', 'no-gutters'])
            let colOne = this.createElement('div', ['col', 'mr-2'])
            let divTitle = this.createElement('div', ['text-uppercase', 'text-primary', 'font-weight-bold', 'text-xs', 'mb-1'])
            let divQuantity = this.createElement('div', ['text-dark', 'font-weight-bold', 'h5', 'mb-0'])

            let colTwo = this.createElement('div', ['col-auto'])
            
            let spanTitle = this.createElement('span', [])
            let spanNumber = this.createElement('span', [])

            // Armar columna 2
            colTwo.innerHTML = category['img'] ? '<i class="fas fa-user-circle fa-2x text-gray-300"></i>' : '<i class="fas fa-table fa-2x text-gray-300"></i>'

            // Insertar datos columna 1
            spanNumber.innerText = 18;
            spanTitle.innerText = category['name']
            
            // Armar columna 1
            divQuantity.append(spanNumber)
            divTitle.append(spanTitle)
            colOne.append(divTitle)
            colOne.append(divQuantity)
            
            // Armar los elementos del div
            
            innerColumns.append(colOne)
            innerColumns.append(colTwo)
            card.append(innerColumns)
            border.append(card)
            column.append(border)
            column.onclick = () => {
                this.getCategory(category['id'])
            }
            this.app.append(column)
        })
    }

    createElement(tag, classNames) {
        const element = document.createElement(tag);
        classNames.forEach(el => {
            element.classList.add(el)
        });

        return element
    }

    getElement(selector) {
        const element = document.querySelector(selector)
    
        return element
    }

    getCategory(id) {
        this.controller.goCategory(id)
    }
}

class Controller{
    initialize(model, view) {
        this.model = model;
        this.view = view;
    }

    retrieve() {
        fetch('https://tllega.jeremiasenriquez.com/api/categories/',
        {
            method: 'GET',
            headers: {
                Authorization: `Token ${localStorage.getItem('key')}`
            },
        })
        .then(res => res.json())
        .then(res => {
            this.model['categories'] = res

            this.view.displayElements();
        })
    }

    goCategory(id) {
        localStorage.setItem('category', id);
        location.href = 'table.html'
    }
}

window.onload = function() {
    const model = new Model();
    const controller = new Controller();
    const view = new View(model, controller);

    controller.initialize(model, view);
}
