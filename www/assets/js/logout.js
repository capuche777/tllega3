document.getElementById('log_out').onclick = function() {
    fetch('https://tllega.jeremiasenriquez.com/api/auth/logout/', {
            method: 'POST',
            body: '',
            headers:{
                'Content-Type': 'application/json'
              }
        })
        .then(res => res.json())
        .catch(err => console.err(err))
        .then(res => {
            localStorage.removeItem('token');
            location.href = '/'
        })
}