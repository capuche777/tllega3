class Model {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
}

class View {
    constructor(model, controller) {
        const self = this;
        const txtEmail = document.getElementById('email');
        const txtPassword = document.getElementById('password');
        const form = document.getElementsByTagName('form')[0];

        self.controller = controller;

        form.onsubmit = function () {
            self.logIn()
        }
    }


    logIn() {
        event.preventDefault();
        const txtEmail = document.getElementById('email');
        const txtPassword = document.getElementById('password');

        const data = {
            email: txtEmail.value,
            password: txtPassword.value,
        }

        this.controller.connect(data)
    }
}

class Controller {
    initialize(model, view) {
        this.model = model;
        this.view = view;
    }

    handleErrors(response) {
        if ( !response.ok ) {
            throw Error(response.statusText);
        }

        return response
    }

    handleErrors(response) {
        if ( !response.ok ) {
            throw Error(response.statusText);
        }

        return response
    }

    connect(data) {
        $.post('https://tllega.jeremiasenriquez.com/api/auth/login/',
        {
            email: data['email'],
            password: data['password']
        }
        ).done(
            function (data, status) {
                localStorage.setItem( 'key', data['key'] )
                location.href = 'dashboard.html'
            }
        ).fail(
            function(xhr, status, error) {
                alert("Por favor verificar usuario y contraseña")
            }
        )

        // fetch('http://http://192.168.1.7:8000/api/auth/login/', {
        //     method: 'POST',
        // })
        // .then(this.handleErrors)
        // .then(res => res.json())
        // .catch(err => console.error(err))
        // .then(res => {
        //     console.log(res['key'])
        // })
    }
}

window.onload = function () {
    const model = new Model();
    const controller = new Controller();
    const view = new View(model, controller);

    controller.initialize(model, view);
}

